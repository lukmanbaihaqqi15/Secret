package com.example.secret.ui.home

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.secret.databinding.FragmentHomeBinding
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

       initView()
        return root
    }

    private fun initView() {
        val database = Firebase.database
        val ref = database.getReference("online").child("isi")
        ref.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                var data = snapshot.value.toString()
                if (data == "1") {
                    mainHandler.post(object : Runnable {
                        override fun run() {
                            var angka = generateAlphanumericString(2)
                            binding.textHome.text = angka
                            mainHandler.postDelayed(this, 5000)
                        }
                    })
                }
                else{
                    mainHandler.post(object : Runnable {
                        override fun run() {
                            var angka = generateAlphanumericString(3)
                            binding.textHome.text = angka
                            mainHandler.postDelayed(this, 5000)
                        }
                    })
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(requireContext(), error.message.toString(), Toast.LENGTH_SHORT).show()
            }

        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    val mainHandler = Handler(Looper.getMainLooper())

    val alphanumeric = ('0'..'9')

    fun generateAlphanumericString(length: Int) : String {
        // The buildString function will create a StringBuilder
        return buildString {
            // We will repeat length times and will append a random character each time
            // This roughly matches how you would do it in plain Java
            repeat(length) { append(alphanumeric.random()) }
        }
    }

}